#!/bin/bash

# Start SSH server
service ssh start

# Start Xvfb
Xvfb :1 -screen 0 1024x768x16 &

# Set the display
export DISPLAY=:1

# Start Unity Hub
unityhub
