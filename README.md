## Register the GitLab Runner:

On a machine where you have GitLab Runner installed (which can be your local machine or any other machine), run the following command to register the GitLab Runner with GitLab. This step generates a config.toml file that contains the runner's configuration.

```
gitlab-runner register \
    --non-interactive \
    --url "https://gitlab.com/" \
    --registration-token "$REGISTRATION_TOKEN" \
    --executor "kubernetes" \
    --name "kubernetes-$HOSTNAME" \
    --run-untagged="true" \
    --tag-list "kubernetes" \
    --kubernetes-image-pull-secret=regret
```

## Create a Dockerfile for the GitLab Runner:

Create a Dockerfile to define the image for your GitLab Runner. This image will include the config.toml file generated during registration, as well as any other dependencies you need.

```Dockerfile
FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
    curl \
    git \
    unzip \
    openssh-client

RUN mkdir -p /etc/gitlab-runner

COPY config.toml /etc/gitlab-runner/config.toml
COPY cacerts.crt /etc/gitlab-runner/cacerts.crt

RUN curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb && \
    dpkg -i gitlab-runner_amd64.deb && \
    rm -f gitlab-runner_amd64.deb

CMD ["/usr/bin/gitlab-runner", "run"]
```

## K8s

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: gitlab-runner
  namespace: gitlab-runner
spec:
  replicas: 1
  selector:
    matchLabels:
      app: gitlab-runner
  template:
    metadata:
      labels:
        app: gitlab-runner
    spec:
      serviceAccountName: gitlab-admin
      containers:
      - name: gitlab-runner
        image: <your-image-repository>/gitlab-runner-image:latest
        imagePullPolicy: Always
        volumeMounts:
        - name: runner-config
          mountPath: /etc/gitlab-runner
          readOnly: true
      imagePullSecrets:
      - name: cafanwi-dockerhub
      volumes:
      - name: runner-config
        configMap:
          name: gitlab-runner-configmap

apiVersion: v1
kind: ConfigMap
metadata:
  name: gitlab-runner-configmap
  namespace: gitlab-runner
data:
  config.toml: |
    # Your GitLab Runner configuration from the registration step goes here
```


## Create and deploy postgres secret 
kubectl -n sonarqube create secret generic sonarqube-postgresql-secret --from-literal=postgres-password=Depay20$ --dry-run=client -o yaml > postgressecret.yaml



## For bitnami/sonarqube
helm repo add sonarqube https://SonarSource.github.io/helm-chart-sonarqube
helm repo update
helm pull sonarqube/sonarqube --untar=true

## configure values.yaml

### change the image to mine
image:
  repository: cafanwii/sonarqube
  tag: 1.0.0
  pullPolicy: IfNotPresent

### [change to true] If enable the JDBC Overwrite, 
jdbcOverwrite:
  # If enable the JDBC Overwrite, make sure to set `postgresql.enabled=false`
  enable: true
  # The JDBC url of the external DB
  jdbcUrl: "jdbc:postgresql://10.0.0.36/sonarqube2024?socketTimeout=1500"
  # The DB user that should be used for the JDBC connection
  jdbcUsername: "cafanwiiuser"
  # Use this if you don't mind the DB password getting stored in plain text within the values file
  jdbcPassword: "Depay20$"

### Enable to deploy the bitnami PostgreSQL chart
enable: false

## install
helm install sonargube sonarqube -n sonarqube

## connecting to database
psql -h cafanwii-postgres-sosotech.io -u cafanwiiuser -d sonarqube

# For sonarqube website
[https://artifacthub.io/packages/helm/sonarqube/sonarqube](https://artifacthub.io/packages/helm/sonarqube/sonarqube)
helm repo add sonarqube https://SonarSource.github.io/helm-chart-sonarqube
helm pull sonarqube/sonarqube --untar=true
helm repo update
kubectl create namespace sonarqube
helm search repo sonarqube/sonarqube --versions
helm upgrade --install sonarqube sonarqube/sonarqube --version 10.1.0+628 -n sonarqube
helm ls -A

```yml
jdbcOverwrite:
  # If enable the JDBC Overwrite, make sure to set `postgresql.enabled=false`
  enable: false
  # The JDBC url of the external DB
  jdbcUrl: "jdbc:postgresql://cafanwii-postgres-sosotech.io/sonarqube?socketTimeout=1500"
  # The DB user that should be used for the JDBC connection
  jdbcUsername: "cafanwiiuser"
  # Use this if you don't mind the DB password getting stored in plain text within the values file
  jdbcPassword: "D20$"
  ## Alternatively, use a pre-existing k8s secret containing the DB password
  # jdbcSecretName: "sonarqube-jdbc"
  ## and the secretValueKey of the password found within that secret
  # jdbcSecretPasswordKey: "jdbc-password"
  ```




