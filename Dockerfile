FROM ubuntu:22.04

# Set environment variables to avoid prompts during package installation
ENV DEBIAN_FRONTEND=noninteractive

# Install necessary packages and dependencies
RUN apt-get update && \
    apt-get install -y \
    wget \
    gpg \
    libgbm1 \
    libasound2 \
    libc6 \
    libcairo2 \
    libcap2 \
    libcups2 \
    libdbus-1-3 \
    libexpat1 \
    libc6-dev \
    libfontconfig1 \
    libfreetype6 \
    libgcc1 \
    libnotify4 \
    libgconf-2-4 \
    libgdk-pixbuf2.0-0 \
    libgl1-mesa-glx \
    libglib2.0-0 \
    libglu1-mesa \
    libgles2-mesa \
    libgtk2.0-0 \
    libgtk-3-0 \
    libnspr4 \
    libncurses5 \
    libnss3 \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libstdc++6 \
    libx11-6 \
    libxcomposite1 \
    libxcursor1 \
    libxdamage1 \
    libxext6 \
    libxfixes3 \
    libxi6 \
    libxrandr2 \
    libxrender1 \
    libxtst6 \
    zlib1g \
    xvfb \
    gnome-keyring \
    x11-apps \
    zenity \
    libxss1 \
    dbus-x11 \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install Unity Hub
RUN wget -qO - https://hub.unity3d.com/linux/keys/public | gpg --dearmor | tee /usr/share/keyrings/Unity_Technologies_ApS.gpg > /dev/null && \
    sh -c 'echo "deb [signed-by=/usr/share/keyrings/Unity_Technologies_ApS.gpg] https://hub.unity3d.com/linux/repos/deb stable main" > /etc/apt/sources.list.d/unityhub.list' && \
    apt-get update && \
    apt-get install -y unityhub && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install libssl packages
RUN wget http://security.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2.22_amd64.deb && \
    wget http://security.ubuntu.com/ubuntu/pool/main/o/openssl/libssl-dev_1.1.1f-1ubuntu2.22_amd64.deb && \
    dpkg -i libssl1.1_1.1.1f-1ubuntu2.22_amd64.deb libssl-dev_1.1.1f-1ubuntu2.22_amd64.deb && \
    apt-get install -f && \
    rm libssl1.1_1.1.1f-1ubuntu2.22_amd64.deb libssl-dev_1.1.1f-1ubuntu2.22_amd64.deb

WORKDIR /root/.config/Unity\ Hub
WORKDIR /opt/unity

COPY UnityHub /usr/bin/
COPY UnityHubCLI /usr/bin/

# Set the entrypoint to run Unity Hub
CMD ["unityhub"]




# FROM eclipse-temurin:17-jre

# LABEL org.opencontainers.image.url=https://github.com/SonarSource/docker-sonarqube

# ENV SONARQUBE_JDBC_URL="jdbc:postgresql://10.0.0.36:5432/sonarqube2024"
# ENV SONARQUBE_JDBC_USERNAME="cafanwiiuser"
# ENV SONARQUBE_JDBC_PASSWORD="sonarqube-postgresql-secret"

# ENV LANG='en_US.UTF-8' \
#     LANGUAGE='en_US:en' \
#     LC_ALL='en_US.UTF-8'

# #
# # SonarQube setup
# #
# ARG SONARQUBE_VERSION=10.4.1.88267
# ARG SONARQUBE_ZIP_URL=https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-${SONARQUBE_VERSION}.zip
# ENV DOCKER_RUNNING="true" \
#     JAVA_HOME='/opt/java/openjdk' \
#     SONARQUBE_HOME=/opt/sonarqube \
#     SONAR_VERSION="${SONARQUBE_VERSION}" \
#     SQ_DATA_DIR="/opt/sonarqube/data" \
#     SQ_EXTENSIONS_DIR="/opt/sonarqube/extensions" \
#     SQ_LOGS_DIR="/opt/sonarqube/logs" \
#     SQ_TEMP_DIR="/opt/sonarqube/temp"

# RUN set -eux; \
#     useradd --system --uid 1000 --gid 0 sonarqube; \
#     apt-get update; \
#     apt-get --no-install-recommends -y install gnupg unzip curl bash fonts-dejavu; \
#     echo "networkaddress.cache.ttl=5" >> "${JAVA_HOME}/conf/security/java.security"; \
#     sed --in-place --expression="s?securerandom.source=file:/dev/random?securerandom.source=file:/dev/urandom?g" "${JAVA_HOME}/conf/security/java.security"; \
#     # pub   2048R/D26468DE 2015-05-25
#     #       Key fingerprint = F118 2E81 C792 9289 21DB  CAB4 CFCA 4A29 D264 68DE
#     # uid                  sonarsource_deployer (Sonarsource Deployer) <infra@sonarsource.com>
#     # sub   2048R/06855C1D 2015-05-25
#     for server in $(shuf -e hkps://keys.openpgp.org \
#                             hkps://keyserver.ubuntu.com) ; do \
#         gpg --batch --keyserver "${server}" --recv-keys 679F1EE92B19609DE816FDE81DB198F93525EC1A && break || : ; \
#     done; \
#     mkdir --parents /opt; \
#     cd /opt; \
#     curl --fail --location --output sonarqube.zip --silent --show-error "${SONARQUBE_ZIP_URL}"; \
#     curl --fail --location --output sonarqube.zip.asc --silent --show-error "${SONARQUBE_ZIP_URL}.asc"; \
#     gpg --batch --verify sonarqube.zip.asc sonarqube.zip; \
#     unzip -q sonarqube.zip; \
#     mv "sonarqube-${SONARQUBE_VERSION}" sonarqube; \
#     rm sonarqube.zip*; \
#     rm -rf ${SONARQUBE_HOME}/bin/*; \
#     ln -s "${SONARQUBE_HOME}/lib/sonar-application-${SONARQUBE_VERSION}.jar" "${SONARQUBE_HOME}/lib/sonarqube.jar"; \
#     chmod -R 550 ${SONARQUBE_HOME}; \
#     chmod -R 770 "${SQ_DATA_DIR}" "${SQ_EXTENSIONS_DIR}" "${SQ_LOGS_DIR}" "${SQ_TEMP_DIR}"; \
#     apt-get remove -y gnupg unzip; \
#     rm -rf /var/lib/apt/lists/*;

# COPY entrypoint.sh ${SONARQUBE_HOME}/docker/
# RUN chmod +x ${SONARQUBE_HOME}/docker/entrypoint.sh


# WORKDIR ${SONARQUBE_HOME}
# EXPOSE 9000

# USER sonarqube
# STOPSIGNAL SIGINT

# ENTRYPOINT ["/opt/sonarqube/docker/entrypoint.sh"]


#############
# FROM ubuntu:latest

# ENV DEBIAN_FRONTEND noninteractive
# ENV TZ=America/New_York

# RUN apt-get update -y \
#     && apt-get install -y \
#         openjdk-11-jdk \
#         postgresql postgresql-contrib \
#         zip \
#         nginx \
#         curl \
#     && apt-get clean \
#     && rm -rf /var/lib/apt/lists/*


# # Download and extract SonarQube
# RUN mkdir -p /sonarqube/ \
#     && cd /sonarqube/ \
#     && curl -O https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-10.0.0.68432.zip \
#     && unzip -o sonarqube-10.0.0.68432.zip -d /opt/ \
#     && mv /opt/sonarqube-10.0.0.68432/ /opt/sonarqube

# # Copy configuration files
# COPY sysctl.conf /root/sysctl.conf_backup
# COPY limits.conf /root/sec_limit.conf_backup
# COPY sonar.properties /opt/sonarqube/conf/sonar.properties
# COPY sonarqube.service /etc/systemd/system/sonarqube.service
# COPY sonarqube.nginx /etc/nginx/sites-available/sonarqube

# # Set up PostgreSQL
# RUN systemctl enable postgresql.service \
#     && systemctl start postgresql.service \
#     && echo "postgres:admin123" | chpasswd \
#     && runuser -l postgres -c "createuser sonar" \
#     && sudo -i -u postgres psql -c "ALTER USER sonar WITH ENCRYPTED PASSWORD 'admin123';" \
#     && sudo -i -u postgres psql -c "CREATE DATABASE sonarqube OWNER sonar;" \
#     && sudo -i -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE sonarqube to sonar;" \
#     && systemctl restart postgresql

# # Set up SonarQube service
# RUN chown -R sonar:sonar /opt/sonarqube/ \
#     && systemctl daemon-reload \
#     && systemctl enable sonarqube.service

# # Set up Nginx
# RUN rm -rf /etc/nginx/sites-enabled/default \
#     && rm -rf /etc/nginx/sites-available/default \
#     && ln -s /etc/nginx/sites-available/sonarqube /etc/nginx/sites-enabled/sonarqube \
#     && systemctl enable nginx.service

# # Expose ports
# EXPOSE 80
# EXPOSE 9000
# EXPOSE 9001

# # Allow necessary ports through firewall
# RUN ufw allow 80,9000,9001/tcp

# # System reboot
# CMD ["bash", "-c", "echo 'System reboot in 30 sec'; sleep 30; reboot"]

# #########################

# Use an image that includes both Python and Apache
# FROM python:latest

# # Install mkdocs and Apache HTTP Server
# RUN pip install mkdocs && \
#     apt-get update && \
#     apt-get install -y apache2 && \
#     rm -rf /var/lib/apt/lists/*

# # Copy the documentation source
# COPY ./sosotech-docs/ /sosotech-docs/

# # Build the documentation using mkdocs
# RUN cd /sosotech-docs && mkdocs build

# # Set up Apache to serve the documentation
# RUN cp -r /sosotech-docs/site/* /var/www/html/

# # Expose port 80 for Apache
# EXPOSE 80

# # Start Apache when the container is run
# CMD ["apachectl", "-D", "FOREGROUND"]

##############
# # Stage 1: Build the documentation
# FROM python:alpine3.17 as builder

# # Copy the documentation source
# COPY ./sosotech-docs/ /sosotech-docs/

# # Install mkdocs and build the documentation
# RUN pip install mkdocs
# RUN cd sosotech-docs && mkdocs build

# # Stage 2: Deploy the built documentation using Apache
# FROM httpd:2.4

# # Copy the built documentation from the previous stage
# COPY --from=builder /sosotech-docs/site/ /usr/local/apache2/htdocs/

##############
# # Use Ubuntu 20.04 as the base image
# FROM ubuntu:20.04

# # Install necessary packages
# RUN apt-get update && \
#     apt-get install -y python3-pip && \
#     pip3 install mkdocs

# # Copy your MkDocs documentation files into the container
# COPY ./sosotech-docs/ /sosotech-docs/

# # Expose port 8000 for serving MkDocs
# EXPOSE 8000

# # Set the working directory
# WORKDIR /sosotech-docs/

# # Initialize a new MkDocs project if needed (uncomment if necessary)
# # RUN mkdocs new sosotech-docs

# # Command to serve the MkDocs documentation
# CMD ["mkdocs", "serve", "--dev-addr=0.0.0.0:8000"]


############
# FROM python:alpine3.17

# COPY ./sosotech-docs/ /sosotech-docs/

# RUN pip install mkdocs

# RUN mkdocs new sosotech-docs

# EXPOSE 8000

# WORKDIR /sosotech-docs/

# ENTRYPOINT ["mkdocs"]
# #CMD [ "python", "server.py" ]
# CMD ["serve", "--dev-addr=0.0.0.0:8000"]
# #CMD ["serve", "mkdocs.yaml"]
